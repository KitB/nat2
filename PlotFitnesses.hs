{-# LANGUAGE NoMonomorphismRestriction #-}
-- | Performs graphing functions
module PlotFitnesses ( chart
                     , chartClassified
                     , chartVaried
                     , toRenderable
                     , renderableToSVGFile
                     , bigCircle
                     , smallCircle
                     )where

import Data.Accessor
import Data.Colour
import Data.Colour.Names
import Graphics.Rendering.Chart
import Math.Statistics

import PSO
import SwarmType
import AnalyseSwarm

-- Diameter of a large circle
bigCircle = 20
-- Diameter of a small circle
smallCircle = 10


-- | Generate AxisData for a given config file
fixedAxis :: SwarmConfig -> t -> AxisData CompType
fixedAxis c _ = makeAxis show ([low,next..high], [], [low,next..high])
    where low  = fromIntegral $ floor $ getMinval c
          high = fromIntegral $ ceiling $ getMaxval c
          next = low + (fromIntegral . floor)  ((high - low) / 10)

-- | Chart a swarm
chart :: SwarmConfig
      -> [Position]
      -> Layout1 Double Double
chart c particles = layout
    where
        pointPlot = plot_points_style  ^= filledCircles 2 (opaque black)
                  $ plot_points_title  ^= "Particles"
                  $ plot_points_values ^= [(p!!0,p!!1)|p <- particles]
                  $ defaultPlotPoints
        layout = layout1_title ^= "Particles"
               $ layout1_left_axis ^: laxis_generate ^= fixedAxis c
               $ layout1_left_axis ^: laxis_title ^= "x1"
               $ layout1_bottom_axis ^: laxis_generate ^= fixedAxis c
               $ layout1_bottom_axis ^: laxis_title ^= "x2"
               $ layout1_plots ^= [Left (toPlot pointPlot)]
               $ layout1_grid_last ^= False
               $ defaultLayout1

-- | Chart a swarm with axes generated for each swarm as we go along
chartVaried :: SwarmConfig
      -> [Position]
      -> Layout1 Double Double
chartVaried c particles = layout
    where
        pointPlot = plot_points_style  ^= filledCircles 2 (opaque black)
                  $ plot_points_title  ^= "Particles"
                  $ plot_points_values ^= [(p!!0,p!!1)|p <- particles]
                  $ defaultPlotPoints
        layout = layout1_title ^= "Particles"
               $ layout1_left_axis ^: laxis_title ^= "x1"
               $ layout1_bottom_axis ^: laxis_title ^= "x2"
               $ layout1_plots ^= [Left (toPlot pointPlot)]
               $ layout1_grid_last ^= False
               $ defaultLayout1

-- | Chart a list of instances of PSO that have been classified
chartClassified :: Double
                -> [(CompType, CompType, Classification)]
                -> Layout1 CompType CompType
chartClassified circleSize classified = layout
  where
    layout      = layout1_title ^= "Classification against parameters"
                $ layout1_left_axis ^: laxis_title ^= "αg (group norm attraction)"
                $ layout1_left_axis ^: laxis_generate ^= axes
                $ layout1_bottom_axis ^: laxis_title ^= "αp (experience or nostalgia)"
                $ layout1_bottom_axis ^: laxis_generate ^= axes
                $ layout1_plots ^= map (Left . toPlot) [converged, oscillating, diverged]
                $ defaultLayout1

    converged   = plot_points_style ^= filledCircles circleSize (opaque green)
                $ plot_points_title ^= "Converged"
                $ plot_points_values ^= [(x,y) | (x,y,c) <- classified, c == Converged]
                $ defaultPlotPoints

    oscillating = plot_points_style ^= filledCircles circleSize (opaque blue)
                $ plot_points_title ^= "Oscillating"
                $ plot_points_values ^= [(x,y) | (x,y,c) <- classified, c == Oscillating]
                $ defaultPlotPoints

    diverged    = plot_points_style ^= filledCircles circleSize (opaque red)
                $ plot_points_title ^= "Diverged"
                $ plot_points_values ^= [(x,y) | (x,y,c) <- classified, c == Diverged]
                $ defaultPlotPoints

    axes _ = makeAxis show ([-0.5,0..4.5], [], [-0.5,0..4.5])
