{-# OPTIONS_GHC -O2 #-}
-- | This module presents a monad representing stateful computation with a stochastic aspect.
--
--   It also contains helper functions for that monad.
module RandomST ( RWorld (..)
                , RandomST
                , makeRandR
                , makeRandRs
                , makeNormalD
                , makeNormalDs
                , setNorm
                , setSwarmBest
                , addSwarm
                , resetSwarms
                , StdGen
                , getStdGen
                )where

import Control.Monad.State
import System.Random
import SwarmType
import Data.Random.Normal

-- | Represents the full state of the world in which a swarm acts
data RWorld = RWorld { getGen :: StdGen           -- ^ A random number generator
                     , getSwarms :: [[Position]]  -- ^ The previous positions of the swarm
                     , getNorm   :: Position      -- ^ The generational best
                     , getSwarmBest   :: Position -- ^ The current best position found
                     }

-- | A type to represent a computation that takes place within an RWorld
type RandomST a = State RWorld a

-- | Generates a random thing inside a range within the RandomST monad
makeRandR :: (Random p) => (p,p) -> RandomST p
makeRandR a = do
    world <- get
    let (p, newgen) = randomR a (getGen world)
    put world { getGen = newgen }
    return p

-- | Generates a list of n random things inside a range within the RandomST monad
makeRandRs :: (Random a) => (a,a) -> Int -> RandomST [a]
makeRandRs a n = replicateM n $ makeRandR a

-- | Generates a random floating-point value from a normal distribution
--   with given mean and variance (in that order)
makeNormalD :: (Random b, Floating b) => b -> b -> RandomST b
makeNormalD μ σ = do
    w@RWorld {getGen = gen} <- get
    let (sample, gen') = normal' (μ, σ) gen
    put w{ getGen = gen' }
    return sample

-- | Generates a list of n random floating point values from a normal distribution
makeNormalDs :: (Random b, Floating b) => Int -> b -> b -> RandomST [b]
makeNormalDs n μ σ = replicateM n $ makeNormalD μ σ


-- | Sets the norm of the world to the given position
setNorm :: Position -> RandomST ()
setNorm p = modify (\w -> w { getNorm = p })

-- | Sets the best position found to a given position
setSwarmBest :: Position -> RandomST ()
setSwarmBest p = modify (\w -> w { getSwarmBest = p })

-- | Adds a swarm's positions to the state so we can get it out at the end to analyse
addSwarm :: [Position] -> RandomST ()
addSwarm s = modify (\w@(RWorld { getSwarms=swarms }) -> w {getSwarms=s:swarms})

-- | Clears the history, to be run when starting a new swarm without
--   leaving the RandomST monad
resetSwarms :: RandomST ()
resetSwarms = modify (\w -> w {getSwarms = []})
