{-# OPTIONS_GHC -O2 #-}
-- | This module attempts to classify swarms as either Converged, Diverged, or Oscillating.
module AnalyseSwarm ( Classification (..)
                    , simplerClassifySwarms
                    , classifySwarms
                    , classify
                    , getElem
                    )where

import SwarmType
import Math.Statistics
import Data.List

-- | A swarm can be in a number of states, these are fairly self explanatory
data Classification = Diverged     -- ^ The particles of the swarm are far apart
                    | Oscillating  -- ^ The particles of the swarm are orbiting
                    | Converged    -- ^ The particles of the swarm have all found the same position
                     deriving (Eq, Ord, Show)

-- | The fitness of a swarm with respect to itself
--
--   That is to say, the average distance for all particles from the average position
--   of all particles
oneSwarmFitness :: [Position] -> CompType
oneSwarmFitness = swarmFitness Nothing

-- | Classifies swarms based upon whether or not they have converged amongst themselves
simplerClassifySwarms :: [[[Position]]] -> Classification
simplerClassifySwarms swarms | f < 0.1 = Converged
                             | f < 50 = Oscillating
                             | f > 50 = Diverged
    where fs = map (oneSwarmFitness . getElem) swarms
          f  = mean fs

-- | Attempts to find a representative state from an instance of an optimisation.
--
--   Currently just presumes that the swarm will show interesting behaviour at
--   around 3/4 of the way through (input list starts at the final state).
--
--   Simplistic and not particularly useful, consider rewrite
getElem ::  [a] -> a
getElem s = s!!((length s)`div`4)

-- | Calculates the average distance of particles in a swarm from either a given point
--   or the mean position of the swarm
swarmFitness :: Maybe Position -> [Position] -> CompType
swarmFitness (Just optimum) swarm = mean . map (distance optimum) $ swarm
swarmFitness Nothing swarm = swarmFitness (Just centrePoint) swarm
    where centrePoint = map mean $ transpose swarm

-- | Calculates the average swarmFitness of a list of swarms
avgFitness :: Maybe Position -> [[Position]] -> CompType
avgFitness optimum swarms = mean $ zipWith (-) (tail fitnesses) fitnesses
    where fitnesses = map (swarmFitness optimum) swarms

-- | Classifies a single swarm based upon either its own mean
--   or a given optimum
classify :: Maybe Position -> [[Position]] -> Classification
classify optimum = classifyCompType . avgFitness optimum

-- | Classifies swarms based upon whether or not they have converged upon a known optimum value
classifySwarms :: Maybe Position -> [[[Position]]] -> Classification
classifySwarms optimum swarms = classifyCompType $ mean $ map (avgFitness optimum) swarms

-- | Takes a score (hopefully distance from a point) and classifies it
classifyCompType :: CompType -> Classification
classifyCompType d | d' < 0 = Converged
                   | d' > 0 = Diverged
                   | otherwise = Oscillating
                    where d' = if abs d < (1*10**(-2)) then 0 else d
