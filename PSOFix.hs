{-# LANGUAGE TypeSynonymInstances, BangPatterns #-}
{-# OPTIONS_GHC -O2 -rtsopts -optc-O3 -optc-ffast-math #-}
-- | This module contains a version of the PSO that does not suffer from the axis-hugging bias
--   that the standard formulation demonstrates.
module PSOFix (
           -- * The entry points
             swarm
           , rSwarm
           ) where

import RandomST
import SwarmType
import Control.Monad.State
import Control.DeepSeq
import Data.Function
import Data.List
import Debug.Trace
import qualified Data.Vector.Unboxed as U

minBy f a b = if f a < f b then a else b

-- | Calculates the next velocity of a particle,
--   given the necessary information
vKPlusOne :: SwarmConfig
          -> Position
          -> CompType -> CompType -> CompType -> CompType -- The randoms
          -> Particle
          -> Velocity
vKPlusOne (Conf { get_ω=ω }) !g !rθp !rθg !rrp !rrg (!xk, !vk, !p) =
        (ω ..* vk)+(vary rθp rrp (p-xk))+(vary rθg rrg (g-xk))
            where toPolar [x, y] = [atan2 y x, sqrt (x**2+y**2)]
                  toCartesian [θ, r] = [r*cos θ, r*sin θ]
                  vary' rθ rr [θ, r] = [θ+rθ, r*rr]
                  vary rθ rr = toCartesian . vary' rθ rr . toPolar
{-# INLINE vKPlusOne #-}

-- | Take a particle and return the next particle
updateParticle :: SwarmConfig
               -> Particle
               -> RandomST Particle
updateParticle c !p =
    do
        RWorld { getNorm = g } <- get
        let (!xk, !vk, !x) = p
        r1 <- makeNormalD 0 (pi/4)
        r2 <- makeNormalD 0 (pi/4)
        r3 <- makeRandR (0, get_αp c)
        r4 <- makeRandR (0, get_αg c)
        let vi = vKPlusOne c g r1 r2 (abs r3) (abs r4) p
        let xi = xk+vi
        let x' = minBy (getFF c) x xi
        return (xi, vi, x')

-- | Turn a swarm into the next swarm
--
--   Sets the norm
updateSwarm :: SwarmConfig
            -> Swarm             -- ^ Positions, velocities, nostalgia
            -> RandomST Swarm    -- ^ the new swarm
updateSwarm c !particles =
    do
        setNorm $ getBest (getFF c) particles
        newParticles <- mapM (updateParticle c) particles -- Generate a new set
        addSwarm $ map (\ (p,_,_) -> p) newParticles
        return newParticles

-- | Find the best position of any particles in a swarm
getBest :: FitnessFunction -> Swarm -> Position
getBest f !ps = minimumBy (compare `on` f) $ map (\(x,_,_)->x) ps

-- | Create an initial swarm at random
initSwarm :: SwarmConfig
          -> RandomST Swarm
initSwarm c@(Conf n m _ mn mx _ _ _ _) = do
    let initS = replicate n $ makeRandRs (mn, mx) m
    start <- sequence initS
    particles <- mapM makeParticle start
    setNorm $ getBest (getFF c) particles
    setSwarmBest $ getBest (getFF c) particles
    addSwarm $ map (\ (p,_,_)->p) particles
    return particles
        where
            makeParticle :: Position -> RandomST Particle
            makeParticle pos = do
                RWorld { getNorm = norm } <- get
                setNorm $ minBy (getFF c) norm pos
                let vel = replicate (length pos) 0
                return (pos, vel, pos)

-- | Take a swarm and apply updateSwarm to it n times
iterateSwarmNTimes :: Int -- ^ Number of times
                   -> Swarm -- ^ Initial swarm
                   -> SwarmConfig
                   -> RandomST Swarm -- ^ Swarm at end
iterateSwarmNTimes 1 s _ = return s
iterateSwarmNTimes n !s c = do
        newSwarm <- updateSwarm c s
        iterateSwarmNTimes (n-1) newSwarm c

-- | Expected point of entry for this module, does everything.
swarm ::  SwarmConfig -> StdGen -> RWorld
swarm c gen = snd $ runState aSwarm (RWorld gen [] [] [])
    where aSwarm = do
              start <- initSwarm c
              iterateSwarmNTimes (getNTimes c) start c
-----

-- | Previously a helper function to swarm, *actually* does all the stuff.
rSwarm :: SwarmConfig
       -> RandomST [[Position]]
rSwarm c = do
    start <- initSwarm c
    iterateSwarmNTimes (getNTimes c) start c
    RWorld { getSwarms = swarms } <- get
    resetSwarms
    return swarms
