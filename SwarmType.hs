{-# OPTIONS_GHC -O2 #-}
-- | This module presents a few type synonyms and helper functions for swarms.
--  
--   It also presents the SwarmConfig data type.
module SwarmType (
                 -- * Types
                   SwarmConfig (..)
                 , Vector
                 , Position
                 , Velocity
                 , FitnessFunction
                 , Swarm
                 , Particle
                 , CompType
                 -- * Functions on those types
                 , (⊗), (⊖), (⊕), (.*), (.-), (.+)
                 , (..*), (../)
                 , limit
                 , distance
                 , vecLength
                 , getPositions
                 ) where

-- | The numeric type to use for computations
type CompType = Double
-- | A vector type
type Vector  = [CompType]
-- | Another name for vector
type Position = Vector
-- | Also another name for vector
type Velocity = Vector
-- | Does what it says on the tin; Tells you what we're optimising here
type FitnessFunction = (Position -> CompType)
-- | A swarm is a list of particles
type Swarm = [Particle]
-- | The basic representation of a particle is
--   its position, its velocity and its previous best
type Particle = (Position, Velocity, Position)


-- | I got really tired of passing all of these things around as arguments to functions
data SwarmConfig = Conf { getSize       :: Int              -- ^ The number of particles in a swarm
                        , getDimensions :: Int              -- ^ The length of each vector
                        , getNTimes     :: Int              -- ^ The number of iterations to perform
                        , getMinval     :: CompType         -- ^ The minimum value to generate initially
                        , getMaxval     :: CompType         -- ^ The maximum value to generate initially
                        , get_ω         :: CompType         -- ^ The momentum or inertial factor
                        , get_αp        :: CompType         -- ^ The weighting for nostalgia/experience
                        , get_αg        :: CompType         -- ^ The weighting for generational best
                        , getFF         :: FitnessFunction  -- ^ The fitness function
                        }

-- | Element-wise multiplication
(⊗) :: Vector -> Vector -> Vector -- You're damn right Haskell can deal with unicode
(⊗) = zipWith (*)
(.*) = (⊗)
-- ^ Convenience function so I can prototype whilst I code
--   I fully intend for all production code to use the unicode

-- | Element-wise subtraction
(⊖) :: Vector -> Vector -> Vector -- But it doesn't allow for overloading + so I'll use a circled operator to mean element-wise
(⊖) = zipWith (-)
(.-) = (⊖)

-- | Element-wise addition
(⊕) :: Vector -> Vector -> Vector
(⊕) = zipWith (+)
(.+) = (⊕)

-- | Scale a vector with a quotient
(../) :: Vector -> CompType -> Vector
xs ../ ω = map (/ω) xs

-- | Scale a vector by a multiple
(..*) :: CompType -> Vector -> Vector
(..*) ω = map (*ω)

-- | Limit the magnitude of a vector
--
--   Specifically for velocities
limit :: Vector -> Vector
--limit xs = if ((sum . map (**2)) xs) > 20 then xs ../ 2 else xs
limit = id

-- | Standard vector norm in ℝⁿ
vecLength :: Vector -> CompType
vecLength = sqrt . sum . map (**2)

-- | Standard vector distance in ℝⁿ
distance :: Vector -> Vector -> CompType
distance v1 v2 = vecLength (v1 ⊖ v2)

-- | Returns the positions from a swarm
getPositions :: Swarm -> [Position]
getPositions = map (\ (p,_,_) -> p)

instance Num a => Num [a] where
    (+) = zipWith (+)
    (-) = zipWith (-)
    (*) = zipWith (*)
    negate = map negate
    abs = map abs
    signum = map signum
    fromInteger = map fromInteger . repeat
