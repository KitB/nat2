{-# OPTIONS_GHC -O2 -rtsopts -optc-O3 -optc-ffast-math #-}
-- | Represents the entry point for execution
module Main where
import AnalyseSwarm
import PlotFitnesses
import PSO
import qualified PSOFix as PF
import SwarmType
import RandomST
import Text.Printf
import qualified Data.Vector.Unboxed as U
import Control.Monad.State(evalState)
import Control.Monad
import Control.DeepSeq
import System.Environment

-- | A sphere config
defaultConfig ::  SwarmConfig
defaultConfig = Conf { getSize       =  25
                     , getDimensions =   2
                     , getNTimes     = 100
                     , getMinval     = -50
                     , getMaxval     =  50
                     , get_ω         =   0.1 -- ω
                     , get_αp        =   2  -- αp
                     , get_αg        =   2       -- αg
                     , getFF         = sum . map (**2) -- Fitness Function
                     }
-- | A config with a much larger swarm (2000) that performs many fewer iterations (25)
bigSwarm ::  SwarmConfig
bigSwarm = defaultConfig { getSize = 2000, getNTimes = 25 }
-- | A config that uses the r function to demonstrate the axis bias of standard PSO
badBigSwarm ::  SwarmConfig
badBigSwarm = bigSwarm {getFF = r 0}

-- | A config for the rastrigin function
rastriginConfig ::  SwarmConfig
rastriginConfig = defaultConfig { getMinval = -5.12
                                , getMaxval =  5.12
                                , getFF      = rastrigin
                                }

-- | The standard rastrigin function (I think)
rastrigin :: Vector -> CompType
rastrigin xs = a `seq` n `seq` (a*n*sum (map (\x -> (x**2)-(a*cos(ω*x))) xs))
    where a = 10; n = fromIntegral $ length xs; ω = 2*pi

-- | A function to demonstrate the rotational effects of PSO
r :: CompType -> Vector -> CompType
r α vec = cos (4*(angle+α)+1)+(sum $ map (**2) vec)
    where a `dot` b = sum $ zipWith (*) a b
          norm a = sqrt (a `dot` a)
          angle = acos $ (vec `dot` unit)/((norm vec)*(norm unit))
          unit = 1:replicate ((length vec)-1) 0

-- | Generate n swarms with given values
doSwarm :: Int -> CompType -> CompType -> RandomST (CompType, CompType, [[[Position]]])
doSwarm n αp αg = do
    let conf = defaultConfig {get_αp = αp, get_αg = αg}
    swarmses <- replicateM n $ rSwarm conf
    return (αp, αg, swarmses)

-- | Generates n swarms for many values of nostalgia and norm
manySwarms :: Int -> RandomST [(CompType, CompType, [[[Position]]])]
manySwarms n = sequence [doSwarm n αp αg | αp <- [0,0.5..4], αg <- [0,0.5..4]]

-- | Generates n sets of swarms for a range on each parameter and graphs results
chartManySwarms :: Int -> IO ()
chartManySwarms n = do
    gen <- getStdGen
    let swarms = evalState (manySwarms n) (RWorld gen [] [] [])
    let classifiedSwarms = swarms `deepseq` map (\ (αp, αg, swarm) -> (αp, αg, simplerClassifySwarms swarm)) swarms
    let renderable = toRenderable $ chartClassified bigCircle classifiedSwarms
    let smallrenderable = toRenderable $ chartClassified smallCircle classifiedSwarms
    renderableToSVGFile renderable 1024 640 "classified_outputs/classifications.svg" 
    renderableToSVGFile smallrenderable 400 300 "classified_outputs/classifications_small.svg" 

-- | Writes an SVG representing the classifications of swarms for various configurations of parameters
mkChart (αp, αg, sets) = do
    let renderable = toRenderable $ chart defaultConfig (getElem $ head sets)
    renderableToSVGFile renderable 400 300 ("classified_outputs/fin-"++show αp++"-"++show αg++"_small.svg")

-- | Writes an SVG representing a swarm
makeChart ::  PrintfArg t => (t, [Position]) -> IO ()
makeChart (i, p) = do
    putStrLn $ "Saving chart: " ++ printf "%03d" i
    let renderable = toRenderable $ chart defaultConfig p
    let varyingAxes = toRenderable $ chartVaried defaultConfig p
    renderableToSVGFile varyingAxes 400 300 (printf "detail_outputs/varyingAxes/img%03d.svg" i)
    renderableToSVGFile renderable 400  300 (printf "detail_outputs/small/img%03d.svg" i)
    renderableToSVGFile renderable 1024 640 (printf "detail_outputs/img%03d.svg" i)


-- | Does a different thing depending upon the first argument to the function
--   options are:
--
--   classify: Makes a chart of classifications of various parameters for swarms,
--   takes an int which is the number of sets to run for each configuration.
--
--   detail: Makes a full set of large, small, and varying axis size graphs for one configuration,
--   takes two doubles, representing the parameters αp and αg
--
--   bigswarm: Same as above but for the bigSwarm configuration
--
--   fixedswarm: Same as above but using the fixed version of PSO
main :: IO ()
main = do
    args <- getArgs
    case head args of
        "classify" -> do putStrLn "Classifying and making examples\nOutputs in classified_outputs/"
                         chartManySwarms (read $ args!!1)
        "detail"   -> do putStrLn $"Detailing swarm with:\nαp\t"++show αp++"\nαg\t"++show αg
                         detailSwarm αp αg
                            where αp = read $ args!!1
                                  αg = read $ args!!2
        "bigswarm" -> do putStrLn $"Detailing big swarm with:\nαp  =  "++show αp++"\nαg  =  "++show αg
                         detailBigSwarm αp αg
                            where αp = read $ args!!1
                                  αg = read $ args!!2
        "fixedswarm" -> do putStrLn $"Detailing big swarm with:\nαp  =  "++show αp++"\nαg  =  "++show αg
                           fixBigSwarm αp αg
                            where αp = read $ args!!1
                                  αg = read $ args!!2

-- | Generates various sizes of graph for one configuration
detailSwarm :: CompType -> CompType -> IO ()
detailSwarm αp αg = do
    gen <- getStdGen
    let mySwarms = reverse $ getSwarms $ swarm (defaultConfig{get_αp=αp, get_αg=αg}) gen
    mapM_ makeChart $ zip ([1..]::[Int]) mySwarms

-- | Generates various sizes of graph for one configuration with bigSwarm
detailBigSwarm :: CompType -> CompType -> IO ()
detailBigSwarm αp αg = do
    gen <- getStdGen
    let mySwarms = reverse $ getSwarms $ swarm (badBigSwarm{get_αp=αp, get_αg=αg}) gen
    mySwarms `deepseq` mapM_ makeChart $ zip ([1..]::[Int]) mySwarms

-- | Generates various sizes of graph for one configuration with bigSwarm using the fixed PSO algorithm
fixBigSwarm :: CompType -> CompType -> IO ()
fixBigSwarm αp αg = do
    gen <- getStdGen
    let mySwarms = reverse $ getSwarms $ PF.swarm (badBigSwarm{get_αp=αp, get_αg=αg}) gen
    mySwarms `deepseq` mapM_ makeChart $ zip ([1..]::[Int]) mySwarms
